/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.duplicatezero;

/**
 *
 * @author brzho
 */
public class DuplicateZero {

    public static void main(String[] args) {
        int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0};
        duplicateZeros(arr);
        System.out.print("[ ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.print("]");
    }

    public static void duplicateZeros(int[] arr) {
        int l = arr.length;         //l = length of arr
        int[] result = new int[l]; 
        int duplicateZeroIndex = 0;

        for (int num : arr) {
            if (num == 0) {
                result[duplicateZeroIndex++] = 0;
                if (duplicateZeroIndex < l) { //not full?
                    result[duplicateZeroIndex++] = 0; //duplicateZero
                }
            } else {
                result[duplicateZeroIndex++] = num; //!=0 use old num
            }
            if (duplicateZeroIndex >= l) {
                break; //stop!!  when full length
            }
        }
        System.arraycopy(result, 0, arr, 0, l);
    }
    
}

